# \QueriesApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**query_period_analyze**](QueriesApi.md#query_period_analyze) | **POST** /queries/{app}/analyze | 分词测试
[**query_period_copy**](QueriesApi.md#query_period_copy) | **POST** /queries/{app}/copy | 复制指定查询并创建新查询
[**query_period_copy_to_query**](QueriesApi.md#query_period_copy_to_query) | **PUT** /queries/{app}/copy | 复制查询到已有查询
[**query_period_create**](QueriesApi.md#query_period_create) | **POST** /queries/{app} | 创建搜索
[**query_period_create_keyword_bindings**](QueriesApi.md#query_period_create_keyword_bindings) | **POST** /queries/{app}/keyword-bindings | 创建新的关键词文档绑定
[**query_period_create_variable**](QueriesApi.md#query_period_create_variable) | **POST** /queries/{app}/variables | 创建新的预定义查询变量
[**query_period_delete**](QueriesApi.md#query_period_delete) | **DELETE** /queries/{app} | 删除搜索
[**query_period_delete_keyword_bindings**](QueriesApi.md#query_period_delete_keyword_bindings) | **DELETE** /queries/{app}/keyword-bindings | 删除关键词文档绑定
[**query_period_delete_variable**](QueriesApi.md#query_period_delete_variable) | **DELETE** /queries/{app}/variables | 删除预定义查询变量
[**query_period_fields**](QueriesApi.md#query_period_fields) | **GET** /queries/{app}/fields | 获取查询关联的所有索引的字段信息
[**query_period_get**](QueriesApi.md#query_period_get) | **GET** /queries/{app}/{query} | 获取查询的详情
[**query_period_get_record**](QueriesApi.md#query_period_get_record) | **GET** /queries/{app}/record | 获取记录的详情
[**query_period_keyword_bindings**](QueriesApi.md#query_period_keyword_bindings) | **GET** /queries/{app}/keyword-bindings | 获取查询的关键词文档绑定列表
[**query_period_list**](QueriesApi.md#query_period_list) | **GET** /queries/{app} | 获取应用下所有索引下的查询列表（按索引进行分组）
[**query_period_profile**](QueriesApi.md#query_period_profile) | **GET** /queries/{app}/profiler | 获取搜索诊断信息
[**query_period_records_of_keyword_binding**](QueriesApi.md#query_period_records_of_keyword_binding) | **GET** /queries/{app}/keyword-bindings-records | 获取关键词绑定对应的记录列表
[**query_period_save_intelligent_mappings**](QueriesApi.md#query_period_save_intelligent_mappings) | **PUT** /queries/{app}/intelligent-mappings | 设置索引智能匹配字段
[**query_period_search**](QueriesApi.md#query_period_search) | **GET** /queries/{app}/search | 搜索测试
[**query_period_source**](QueriesApi.md#query_period_source) | **POST** /queries/{app}/{query} | 获取最终查询的源码(JSON)
[**query_period_suggest**](QueriesApi.md#query_period_suggest) | **GET** /queries/{app}/suggest | 获取搜索建议列表
[**query_period_test_intelligent_mappings**](QueriesApi.md#query_period_test_intelligent_mappings) | **POST** /queries/{app}/intelligent-mappings | 测试索引智能匹配字段
[**query_period_update**](QueriesApi.md#query_period_update) | **PUT** /queries/{app} | 修改查询
[**query_period_update_keyword_bindings**](QueriesApi.md#query_period_update_keyword_bindings) | **PATCH** /queries/{app}/keyword-bindings | 修改关键词文档绑定
[**query_period_update_settings**](QueriesApi.md#query_period_update_settings) | **POST** /queries/{app}/settings | 更改查询的设置项
[**query_period_update_variable**](QueriesApi.md#query_period_update_variable) | **PATCH** /queries/{app}/variables | 修改预定义查询变量
[**query_period_validate**](QueriesApi.md#query_period_validate) | **GET** /queries/{app}/validate | 获取搜索验证结果
[**query_period_validate_aggregation**](QueriesApi.md#query_period_validate_aggregation) | **POST** /queries/{app}/validate-aggregation | 验证聚合定义是否正确
[**query_period_validate_query**](QueriesApi.md#query_period_validate_query) | **POST** /queries/{app}/validate-query | 验证聚合定义是否正确
[**query_period_validate_script_field**](QueriesApi.md#query_period_validate_script_field) | **POST** /queries/{app}/validate-script-field | 验证脚本字段是否正确
[**query_period_validate_script_score**](QueriesApi.md#query_period_validate_script_score) | **POST** /queries/{app}/validate-script-score | 验证脚本字段是否正确
[**query_period_validate_suggestion**](QueriesApi.md#query_period_validate_suggestion) | **POST** /queries/{app}/validate-suggest | 验证建议是否正确
[**query_period_variables**](QueriesApi.md#query_period_variables) | **GET** /queries/{app}/variables | 获取应用的预定义查询变量列表



## query_period_analyze

> Vec<crate::models::AnalyzeToken> query_period_analyze(app, analyze_object, index)
分词测试

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**analyze_object** | [**AnalyzeObject**](AnalyzeObject.md) |  | [required] |
**index** | Option<**f32**> | 索引编号，如果指定索引编号则使用索引的分词器 |  |[default to 0]

### Return type

[**Vec<crate::models::AnalyzeToken>**](AnalyzeToken.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_copy

> crate::models::QueryBean query_period_copy(app, query)
复制指定查询并创建新查询

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 源查询编号 | [required] |

### Return type

[**crate::models::QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_copy_to_query

> bool query_period_copy_to_query(app, query, to)
复制查询到已有查询

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 源查询编号 | [required] |
**to** | **i32** | 目标查询编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_create

> crate::models::QueryBean query_period_create(app, query_form)
创建搜索

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query_form** | [**QueryForm**](QueryForm.md) |  | [required] |

### Return type

[**crate::models::QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_create_keyword_bindings

> crate::models::KeywordBindingBean query_period_create_keyword_bindings(app, query, keyword_binding_bean)
创建新的关键词文档绑定

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**keyword_binding_bean** | [**KeywordBindingBean**](KeywordBindingBean.md) |  | [required] |

### Return type

[**crate::models::KeywordBindingBean**](KeywordBindingBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_create_variable

> crate::models::QueryVariableBean query_period_create_variable(app, query_variable_bean)
创建新的预定义查询变量



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query_variable_bean** | [**QueryVariableBean**](QueryVariableBean.md) |  | [required] |

### Return type

[**crate::models::QueryVariableBean**](QueryVariableBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_delete

> bool query_period_delete(app, query)
删除搜索

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_delete_keyword_bindings

> bool query_period_delete_keyword_bindings(app, query, id)
删除关键词文档绑定

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**id** | **i32** | 关键词编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_delete_variable

> bool query_period_delete_variable(app, id)
删除预定义查询变量



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | 自定义查询变量编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_fields

> Vec<crate::models::IndexFieldBean> query_period_fields(app, query)
获取查询关联的所有索引的字段信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |

### Return type

[**Vec<crate::models::IndexFieldBean>**](IndexFieldBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_get

> crate::models::QueryBean query_period_get(app, query)
获取查询的详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |

### Return type

[**crate::models::QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_get_record

> serde_json::Value query_period_get_record(app, query, _id)
获取记录的详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**_id** | **String** | 记录 _id 值 | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_keyword_bindings

> Vec<crate::models::KeywordBindingBean> query_period_keyword_bindings(app, query)
获取查询的关键词文档绑定列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |

### Return type

[**Vec<crate::models::KeywordBindingBean>**](KeywordBindingBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_list

> Vec<crate::models::QueryBean> query_period_list(app, index)
获取应用下所有索引下的查询列表（按索引进行分组）

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | Option<**i32**> | 索引编号 |  |

### Return type

[**Vec<crate::models::QueryBean>**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_profile

> serde_json::Value query_period_profile(app, query, q)
获取搜索诊断信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 记录编号 | [required] |
**q** | Option<**String**> | 诊断关键字 |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_records_of_keyword_binding

> Vec<serde_json::Value> query_period_records_of_keyword_binding(app, id)
获取关键词绑定对应的记录列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | 关键词绑定编号 | [required] |

### Return type

[**Vec<serde_json::Value>**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_save_intelligent_mappings

> bool query_period_save_intelligent_mappings(app, query, fields)
设置索引智能匹配字段

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**fields** | Option<[**Vec<String>**](String.md)> | 字段列表 |  |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_search

> serde_json::Value query_period_search(app, query, from, size, q, params)
搜索测试

该接口主要用于定制查询的测试，必须授权才能访问

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**from** | **i32** | 起始记录 | [required] |[default to 0]
**size** | **i32** | 每页记录数量 | [required] |[default to 10]
**q** | Option<**String**> | 查询关键字 |  |
**params** | Option<[**::std::collections::HashMap<String, String>**](String.md)> | 聚合参数 |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_source

> serde_json::Value query_period_source(app, query, q)
获取最终查询的源码(JSON)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**q** | Option<**String**> | 搜索关键字 |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_suggest

> Vec<serde_json::Value> query_period_suggest(app, query, q)
获取搜索建议列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**q** | **String** | 搜索关键字 | [required] |

### Return type

[**Vec<serde_json::Value>**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_test_intelligent_mappings

> Vec<crate::models::IntelligentMapping> query_period_test_intelligent_mappings(app, query, q, fields)
测试索引智能匹配字段

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**q** | **String** | 搜索内容 | [required] |
**fields** | Option<[**Vec<String>**](String.md)> | 字段列表 |  |

### Return type

[**Vec<crate::models::IntelligentMapping>**](IntelligentMapping.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_update

> crate::models::QueryBean query_period_update(app, query, query_form)
修改查询

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**query_form** | [**QueryForm**](QueryForm.md) |  | [required] |

### Return type

[**crate::models::QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_update_keyword_bindings

> crate::models::KeywordBindingBean query_period_update_keyword_bindings(app, query, keyword_binding_bean)
修改关键词文档绑定

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**keyword_binding_bean** | [**KeywordBindingBean**](KeywordBindingBean.md) |  | [required] |

### Return type

[**crate::models::KeywordBindingBean**](KeywordBindingBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_update_settings

> bool query_period_update_settings(app, query, name, value, r#type)
更改查询的设置项

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 记录编号 | [required] |
**name** | **String** | 设置项名称 | [required] |
**value** | **String** | 设置值 | [required] |
**r#type** | **String** | 设置项类型 | [required] |[default to string]

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_update_variable

> crate::models::QueryVariableBean query_period_update_variable(app, id, query_variable_bean)
修改预定义查询变量



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | 自定义查询变量编号 | [required] |
**query_variable_bean** | [**QueryVariableBean**](QueryVariableBean.md) |  | [required] |

### Return type

[**crate::models::QueryVariableBean**](QueryVariableBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_validate

> serde_json::Value query_period_validate(app, query)
获取搜索验证结果

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_validate_aggregation

> serde_json::Value query_period_validate_aggregation(app, query, body)
验证聚合定义是否正确

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**body** | **serde_json::Value** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_validate_query

> serde_json::Value query_period_validate_query(app, query, body)
验证聚合定义是否正确

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**body** | **serde_json::Value** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_validate_script_field

> serde_json::Value query_period_validate_script_field(app, body, query, index)
验证脚本字段是否正确

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**body** | **serde_json::Value** |  | [required] |
**query** | Option<**i32**> | 查询编号, query 和 index 两个参数传一个即可 |  |
**index** | Option<**i32**> | 索引编号, query 和 index 两个参数传一个即可 |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_validate_script_score

> serde_json::Value query_period_validate_script_score(app, query, body)
验证脚本字段是否正确

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**body** | **serde_json::Value** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_validate_suggestion

> serde_json::Value query_period_validate_suggestion(app, query, body)
验证建议是否正确

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**body** | **serde_json::Value** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## query_period_variables

> Vec<crate::models::QueryVariableBean> query_period_variables(app)
获取应用的预定义查询变量列表



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::QueryVariableBean>**](QueryVariableBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# CrawlerTask

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | Option<**i32**> |  | [optional]
**template** | Option<**String**> |  | [optional]
**period** | Option<**i32**> |  | [optional]
**settings** | Option<[**serde_json::Value**](.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**run_times** | Option<**i32**> |  | [optional]
**last_updated_at** | Option<**String**> |  | [optional]
**last_error** | Option<**String**> |  | [optional]
**error_count** | Option<**i32**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



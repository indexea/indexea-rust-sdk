# IndexForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> |  | [optional]
**intro** | Option<**String**> |  | [optional]
**alias** | Option<**Vec<String>**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**shards** | Option<**i32**> | 该索引的分片数 | [optional]
**replicas** | Option<**i32**> | 该索引的副本数 | [optional]
**analyzer** | Option<**String**> |  | [optional]
**search_analyzer** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



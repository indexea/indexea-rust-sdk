# \FieldsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**index_period_fields**](FieldsApi.md#index_period_fields) | **GET** /indices/{app}/{index}/fields | 获取索引字段映射详情
[**index_period_update_fields**](FieldsApi.md#index_period_update_fields) | **POST** /indices/{app}/{index}/fields | 更新索引的字段映射
[**index_period_update_html_strip_fields**](FieldsApi.md#index_period_update_html_strip_fields) | **PATCH** /indices/{app}/{index}/fields | 更新索引的HTML过滤字段列表
[**index_period_values_of_field**](FieldsApi.md#index_period_values_of_field) | **GET** /indices/{app}/{index}/fields/{field} | 获取索引字段的所有值列表



## index_period_fields

> serde_json::Value index_period_fields(app, index)
获取索引字段映射详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_update_fields

> serde_json::Value index_period_update_fields(app, index, body)
更新索引的字段映射

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**body** | **serde_json::Value** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_update_html_strip_fields

> serde_json::Value index_period_update_html_strip_fields(app, index, body)
更新索引的HTML过滤字段列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**body** | **serde_json::Value** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_values_of_field

> Vec<crate::models::ValueOfField> index_period_values_of_field(app, index, field, size)
获取索引字段的所有值列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**field** | **String** | 字段名称 | [required] |
**size** | **i32** | values count | [required] |[default to 20]

### Return type

[**Vec<crate::models::ValueOfField>**](ValueOfField.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


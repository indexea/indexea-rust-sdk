# IndexRebuildForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shards** | Option<**i32**> | 该索引的分片数 | [optional]
**replicas** | Option<**i32**> | 该索引的副本数 | [optional]
**analyzer** | Option<**String**> |  | [optional]
**search_analyzer** | Option<**String**> |  | [optional]
**fields** | Option<[**serde_json::Value**](.md)> |  | [optional]
**vcode** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



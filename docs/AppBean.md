# AppBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**account** | Option<**i32**> | 创建者 | [optional]
**cluster** | Option<**i32**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**ident** | Option<**String**> | 应用唯一标识 | [optional]
**intro** | Option<**String**> |  | [optional]
**paid** | Option<**bool**> | 是否付费应用 | [optional]
**storage** | Option<**i32**> | 存储空间 | [optional]
**shards** | Option<**i32**> | 分片数 | [optional]
**replicas** | Option<**i32**> | 副本数 | [optional]
**searchs** | Option<**i32**> | 搜索次数 | [optional]
**mentor** | Option<[**crate::models::AppMentorBean**](AppMentorBean.md)> |  | [optional]
**settings** | Option<[**serde_json::Value**](.md)> | 应用设置 | [optional]
**indices** | Option<**i32**> | 应用索引数 | [optional]
**created_at** | Option<**String**> |  | [optional]
**expired_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



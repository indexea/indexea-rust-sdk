# IndexFieldBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | Option<[**crate::models::IndexBean**](.md)> |  | [optional]
**fields** | Option<[**serde_json::Value**](.md)> | 字段列表信息 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \MessageApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**message_period_delete**](MessageApi.md#message_period_delete) | **DELETE** /accounts/message | 删除消息
[**message_period_feedback**](MessageApi.md#message_period_feedback) | **POST** /accounts/feedback | 反馈意见
[**message_period_list**](MessageApi.md#message_period_list) | **GET** /accounts/message | 获取我相关的消息信息，包括未读消息数量、最新消息等
[**message_period_read**](MessageApi.md#message_period_read) | **PATCH** /accounts/message | 标识消息为已读
[**message_period_send**](MessageApi.md#message_period_send) | **POST** /accounts/message | 发送消息



## message_period_delete

> bool message_period_delete(id)
删除消息

删除消息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **i32** | 消息编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## message_period_feedback

> bool message_period_feedback(content, r#type)
反馈意见

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**content** | **String** |  | [required] |
**r#type** | **String** |  | [required] |[default to general]

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## message_period_list

> crate::models::Messages message_period_list(scope, size, from)
获取我相关的消息信息，包括未读消息数量、最新消息等

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**scope** | **String** |  | [required] |[default to unread]
**size** | **i32** | 消息数量 | [required] |[default to 10]
**from** | Option<**i32**> | 用于翻页的起始位置 |  |[default to 0]

### Return type

[**crate::models::Messages**](Messages.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## message_period_read

> crate::models::Messages message_period_read(id)
标识消息为已读

标识消息为已读

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | 消息编号,多个消息使用逗号隔开 | [required] |

### Return type

[**crate::models::Messages**](Messages.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## message_period_send

> crate::models::Message message_period_send(receiver, msg)
发送消息

发送站内消息给某人

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**receiver** | **i32** |  | [required] |
**msg** | **String** |  | [required] |

### Return type

[**crate::models::Message**](Message.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


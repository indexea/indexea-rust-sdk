# AccountBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i64**> |  | [optional]
**scopes** | Option<**String**> | 如果是第三方应用，此值为三方应用的权限 | [optional]
**token** | Option<**String**> | 登录令牌(执行登录方法时该值为可用的 token) | [optional]
**token_at** | Option<**String**> | token 的有效时间 | [optional]
**name** | Option<**String**> |  | [optional]
**account** | Option<**String**> |  | [optional]
**avatar** | Option<**String**> |  | [optional]
**company** | Option<**String**> |  | [optional]
**nation** | Option<**String**> |  | [optional]
**state** | Option<**String**> |  | [optional]
**city** | Option<**String**> |  | [optional]
**address** | Option<**String**> |  | [optional]
**login_ip** | Option<**String**> |  | [optional]
**login_at** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**origin** | Option<**String**> | 账号来源 | [optional]
**subscription** | Option<**i32**> |  | [optional]
**settings** | Option<[**serde_json::Value**](.md)> |  | [optional]
**roles** | Option<**Vec<String>**> |  | [optional]
**status** | Option<**i32**> |  | [optional]
**apps** | Option<[**Vec<crate::models::AppBean>**](AppBean.md)> | 应用列表 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# CrawlerLog

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**home_url** | Option<**String**> |  | [optional]
**begin_at** | Option<**String**> |  | [optional]
**end_at** | Option<**String**> |  | [optional]
**used_seconds** | Option<**i32**> |  | [optional]
**success_count** | Option<**i32**> |  | [optional]
**failed_count** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



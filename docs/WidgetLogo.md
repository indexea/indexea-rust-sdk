# WidgetLogo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logo** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



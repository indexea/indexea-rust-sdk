# RecordFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | Option<[**Vec<serde_json::Value>**](serde_json::Value.md)> | 过滤条件 | [optional]
**aggs** | Option<[**serde_json::Value**](.md)> | 聚合字段 | [optional]
**sorts** | Option<[**Vec<serde_json::Value>**](serde_json::Value.md)> | 排序字段 | [optional]
**post_filters** | Option<[**serde_json::Value**](.md)> | 聚合过滤条件 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



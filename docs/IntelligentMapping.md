# IntelligentMapping

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**word** | Option<**String**> | 关键词 | [optional]
**field** | Option<**String**> | 匹配字段 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



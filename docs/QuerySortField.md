# QuerySortField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | Option<**String**> | 排序字段名 | [optional]
**label** | Option<**String**> | 排序字段在前端显示的名称 | [optional]
**order** | Option<**String**> | 默认排序方式 | [optional][default to Desc]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



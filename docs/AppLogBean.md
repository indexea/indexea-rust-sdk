# AppLogBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**account** | Option<[**crate::models::AppLogAccount**](AppLogAccount.md)> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**widget** | Option<**i32**> |  | [optional]
**query** | Option<**i32**> |  | [optional]
**recomm** | Option<**i32**> |  | [optional]
**r#type** | Option<**i32**> | 日志类型值 | [optional]
**type_desc** | Option<**String**> | 日志类型说明 | [optional]
**log** | Option<**String**> | 日志内容 | [optional]
**detail** | Option<[**serde_json::Value**](.md)> |  | [optional]
**ip** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Bulletin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**level** | Option<**String**> |  | [optional]
**scope** | Option<**i32**> |  | [optional]
**title** | Option<**String**> |  | [optional]
**content** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**start_at** | Option<**String**> |  | [optional]
**expired_at** | Option<**String**> |  | [optional]
**closable** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# SearchEstimateResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_purchase_searchs** | Option<**i32**> |  | [optional]
**last_purchase_searchs** | Option<**i32**> |  | [optional]
**avail_searchs** | Option<**i32**> |  | [optional]
**average_searchs** | Option<**i32**> |  | [optional]
**total_searchs** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# AppMentorBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | Option<**i32**> | 应用编号 | [optional]
**account** | Option<**i32**> | 成员编号 | [optional]
**name** | Option<**String**> | 成员备注名 | [optional]
**app_name** | Option<**String**> | 成员对应用的备注名 | [optional]
**scopes** | Option<**String**> | 成员权限 | [optional]
**report** | Option<**i32**> | 是否接收报告 | [optional]
**settings** | Option<[**serde_json::Value**](.md)> | 成员的设置项 | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



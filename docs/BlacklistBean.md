# BlacklistBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | Option<**i32**> |  | [optional]
**block_type** | Option<**i32**> |  | [optional]
**badwords** | Option<**Vec<String>**> |  | [optional]
**black_ips** | Option<**Vec<String>**> |  | [optional]
**white_ips** | Option<**Vec<String>**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



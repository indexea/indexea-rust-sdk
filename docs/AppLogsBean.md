# AppLogsBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | Option<**i32**> | 日志总数 | [optional]
**logs** | Option<[**Vec<crate::models::AppLogBean>**](AppLogBean.md)> |  | [optional]
**excel_url** | Option<**String**> | 用于导出 Excel 的地址 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



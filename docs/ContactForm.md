# ContactForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **String** |  | 
**name** | **String** |  | 
**tel** | **String** |  | 
**email** | Option<**String**> |  | [optional]
**memo** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# PaymentRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**ident** | Option<**String**> |  | [optional]
**order_id** | Option<**i32**> |  | [optional]
**r#type** | Option<**i32**> |  | [optional]
**amount** | Option<**i32**> |  | [optional]
**payment** | Option<**i32**> |  | [optional]
**intro** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



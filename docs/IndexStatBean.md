# IndexStatBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | Option<**String**> |  | [optional]
**index** | Option<**String**> |  | [optional]
**health** | Option<**String**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**records** | Option<**i32**> |  | [optional]
**storage** | Option<**i32**> |  | [optional]
**shards** | Option<**i32**> |  | [optional]
**replicas** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



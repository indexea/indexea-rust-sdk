# \SearchApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search_period_click**](SearchApi.md#search_period_click) | **POST** /search/widget/{widget}/click | 搜索结果点击行为收集
[**search_period_histories**](SearchApi.md#search_period_histories) | **GET** /search/widget/{widget}/histories | 获取当前搜索用户的最新搜索记录
[**search_period_logs**](SearchApi.md#search_period_logs) | **GET** /apps/{app}/logs-searchs | 获取搜索日志
[**search_period_query_histories**](SearchApi.md#search_period_query_histories) | **GET** /search/query/{query}/histories | 获取当前搜索用户的最新搜索记录
[**search_period_query_hot_words**](SearchApi.md#search_period_query_hot_words) | **GET** /search/query/{query}/hotwords | 获取查询相关热词
[**search_period_query_search**](SearchApi.md#search_period_query_search) | **GET** /search/query/{query} | 基于查询的公开搜索
[**search_period_widget_auto_complete**](SearchApi.md#search_period_widget_auto_complete) | **GET** /search/widget/{widget}/autocomplete | 基于组件的搜索词自动完成
[**search_period_widget_hot_words**](SearchApi.md#search_period_widget_hot_words) | **GET** /search/widget/{widget}/hotwords | 获取组件搜索的相关热词
[**search_period_widget_search**](SearchApi.md#search_period_widget_search) | **GET** /search/widget/{widget} | 基于组件的公开搜索



## search_period_click

> bool search_period_click(widget, action_id, doc_id, userid, x_token)
搜索结果点击行为收集

该接口主要用于记录用户对搜索结果的点击行为

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**widget** | **String** | 组件唯一标识 | [required] |
**action_id** | **String** | 对应搜索行为编号 | [required] |
**doc_id** | **String** | 对应索引中的内部记录编号 | [required] |
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_histories

> Vec<String> search_period_histories(widget, strategy, query, size, userid, x_token)
获取当前搜索用户的最新搜索记录

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**widget** | **String** | 组件唯一标识 | [required] |
**strategy** | **String** | 搜索记录策略 | [required] |[default to popular]
**query** | **i32** | 指定关联查询的编号 | [required] |[default to 0]
**size** | **i32** | 数量 | [required] |[default to 10]
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**x_token** | Option<**String**> | 如果要使用非公开的组件，需要组件作者授权 |  |

### Return type

**Vec<String>**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_logs

> Vec<crate::models::QueryActionBean> search_period_logs(app, indices, scope, widget, query, recomm, start_date, end_date, from, size)
获取搜索日志

该接口主要用于获取搜索明细

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**indices** | Option<[**Vec<i32>**](i32.md)> | 只看指定索引 |  |
**scope** | Option<**String**> | 搜索范围 |  |[default to all]
**widget** | Option<**i32**> | 搜索组件 |  |[default to 0]
**query** | Option<**i32**> | 指定查询 |  |[default to 0]
**recomm** | Option<**i32**> | 推荐组件 |  |[default to 0]
**start_date** | Option<**String**> | 统计起始日期 |  |
**end_date** | Option<**String**> | 统计结束日期 |  |
**from** | Option<**i32**> | 起始位置 |  |[default to 0]
**size** | Option<**i32**> | 每页记录数量 |  |[default to 50]

### Return type

[**Vec<crate::models::QueryActionBean>**](QueryActionBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_query_histories

> Vec<String> search_period_query_histories(query, strategy, size, userid, x_token)
获取当前搜索用户的最新搜索记录

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**query** | **String** | 查询唯一标识 | [required] |
**strategy** | **String** | 搜索记录策略 | [required] |[default to popular]
**size** | **i32** | 数量 | [required] |[default to 10]
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**x_token** | Option<**String**> | 如果要使用非公开查询，需要组件作者授权 |  |

### Return type

**Vec<String>**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_query_hot_words

> Vec<crate::models::SearchWord> search_period_query_hot_words(query, scope, count, x_token, userid)
获取查询相关热词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**query** | **String** | 查询标识 | [required] |
**scope** | **String** | 时间范围 | [required] |[default to all]
**count** | **i32** | 获取热词数量 | [required] |[default to 10]
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |

### Return type

[**Vec<crate::models::SearchWord>**](SearchWord.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_query_search

> serde_json::Value search_period_query_search(query, from, size, x_token, userid, q, params)
基于查询的公开搜索

该接口主要用于公开搜索，如果查询是公开的就不需要授权

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**query** | **String** | 查询标识 | [required] |
**from** | **i32** | 起始记录 | [required] |[default to 0]
**size** | **i32** | 每页记录数量 | [required] |[default to 20]
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**q** | Option<**String**> | 搜索关键字 |  |
**params** | Option<[**::std::collections::HashMap<String, String>**](String.md)> | 聚合参数 |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_widget_auto_complete

> Vec<crate::models::AutoCompleteItem> search_period_widget_auto_complete(widget, query, q, size, userid, x_token)
基于组件的搜索词自动完成

该接口主要为搜索输入框提供自动完成的功能

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**widget** | **String** | 组件唯一标识 | [required] |
**query** | **i32** | 查询编号 | [required] |[default to 0]
**q** | **String** | 搜索关键字 | [required] |
**size** | **i32** | 数量 | [required] |[default to 10]
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |

### Return type

[**Vec<crate::models::AutoCompleteItem>**](AutoCompleteItem.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_widget_hot_words

> Vec<crate::models::SearchWord> search_period_widget_hot_words(widget, scope, count, x_token, userid, query)
获取组件搜索的相关热词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**widget** | **String** | 组件唯一标识 | [required] |
**scope** | **String** | 时间范围 | [required] |[default to all]
**count** | **i32** | 获取热词数量 | [required] |[default to 10]
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**query** | Option<**i32**> | 查询编号 |  |

### Return type

[**Vec<crate::models::SearchWord>**](SearchWord.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_period_widget_search

> serde_json::Value search_period_widget_search(widget, query, from, size, userid, x_token, original, q, params)
基于组件的公开搜索

该接口主要为UI组件提供公开搜索

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**widget** | **String** | 组件唯一标识 | [required] |
**query** | **i32** | 查询编号 | [required] |
**from** | **i32** | 起始记录 | [required] |[default to 0]
**size** | **i32** | 每页记录数量, 如果值小于0则使用预设值的记录数 | [required] |[default to -1]
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |
**original** | Option<**String**> | 搜索动作的延续，在 Web 组件中一边输入即时搜索时，使用的是同一个 original，original 值等于第一个搜索动作产生结果中的 action 值 |  |
**q** | Option<**String**> | 搜索关键字 |  |
**params** | Option<[**::std::collections::HashMap<String, String>**](String.md)> | 聚合参数 |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


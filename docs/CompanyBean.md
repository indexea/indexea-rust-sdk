# CompanyBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> | 公司名称 | [optional]
**url** | Option<**String**> | 公司网址 | [optional]
**nation** | Option<**String**> | 国家 | [optional]
**province** | Option<**String**> | 省/州/地区 | [optional]
**city** | Option<**String**> | 城市 | [optional]
**taxpayer** | Option<**String**> | 纳税人识别号 | [optional]
**bank** | Option<**String**> | 开户行 | [optional]
**account** | Option<**String**> | 银行账号 | [optional]
**address** | Option<**String**> | 公司注册地址 | [optional]
**tel** | Option<**String**> | 公司电话 | [optional]
**license** | Option<**String**> | 营业执照图片地址 | [optional]
**certificate** | Option<**String**> | 一般纳税人证明扫描件图片地址 | [optional]
**post_addr** | Option<**String**> | 快递地址 | [optional]
**post_code** | Option<**String**> | 邮编 | [optional]
**post_name** | Option<**String**> | 收件人姓名 | [optional]
**post_tel** | Option<**String**> | 收件人电话 | [optional]
**created_at** | Option<**String**> | 首次填写时间 | [optional]
**updated_at** | Option<**String**> | 最后更新时间 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



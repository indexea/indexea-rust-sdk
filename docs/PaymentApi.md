# \PaymentApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**payment_period_alipay**](PaymentApi.md#payment_period_alipay) | **POST** /payment/alipay | 接受支付宝的支付结果
[**payment_period_alipay_return**](PaymentApi.md#payment_period_alipay_return) | **GET** /payment/alipay | 支付宝平台支付完毕后调整到该接口
[**payment_period_apply_invoice**](PaymentApi.md#payment_period_apply_invoice) | **POST** /payment/{app}/invoices | 申请发票
[**payment_period_begin_pay**](PaymentApi.md#payment_period_begin_pay) | **POST** /payment/{app}/orders/{ident} | 支付订单
[**payment_period_buy**](PaymentApi.md#payment_period_buy) | **PUT** /payment/{app}/orders | 下单购买
[**payment_period_delete_invoice**](PaymentApi.md#payment_period_delete_invoice) | **DELETE** /payment/{app}/invoices | 删除发票
[**payment_period_delete_order**](PaymentApi.md#payment_period_delete_order) | **DELETE** /payment/{app}/orders/{ident} | 取消订单
[**payment_period_invoices**](PaymentApi.md#payment_period_invoices) | **GET** /payment/{app}/invoices | 获取发票列表
[**payment_period_order**](PaymentApi.md#payment_period_order) | **GET** /payment/{app}/orders/{ident} | 获取订单详情
[**payment_period_orders**](PaymentApi.md#payment_period_orders) | **POST** /payment/{app}/orders | 订单列表
[**payment_period_orders_without_invoice**](PaymentApi.md#payment_period_orders_without_invoice) | **GET** /payment/{app}/orders_without_invoice | 获取未曾开票的订单列表
[**payment_period_price**](PaymentApi.md#payment_period_price) | **GET** /payment/{app}/price | 获取套餐价格
[**payment_period_receipt**](PaymentApi.md#payment_period_receipt) | **GET** /payment/{app}/orders | 获取订单回执图片
[**payment_period_request_contact**](PaymentApi.md#payment_period_request_contact) | **POST** /payment/{app}/contact | 联系销售获取私有化报价
[**payment_period_upload_receipt**](PaymentApi.md#payment_period_upload_receipt) | **PUT** /payment/{app}/orders/{ident} | 上传转账回执
[**payment_period_wepay**](PaymentApi.md#payment_period_wepay) | **POST** /payment/wepay | 接受微信支付的支付结果



## payment_period_alipay

> payment_period_alipay()
接受支付宝的支付结果

### Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_alipay_return

> payment_period_alipay_return()
支付宝平台支付完毕后调整到该接口

### Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_apply_invoice

> crate::models::PaymentInvoice payment_period_apply_invoice(app, r#type, request_body)
申请发票

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**r#type** | **i32** | 发票类型 | [required] |[default to 1]
**request_body** | [**Vec<String>**](String.md) | 申请开发票的订单编号列表 | [required] |

### Return type

[**crate::models::PaymentInvoice**](PaymentInvoice.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_begin_pay

> crate::models::PayResult payment_period_begin_pay(app, ident, r#type)
支付订单

选择支付方式，开始支付

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 订单号 | [required] |
**r#type** | **String** | 支付方式 | [required] |

### Return type

[**crate::models::PayResult**](PayResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_buy

> crate::models::PaymentOrder payment_period_buy(app, payment_service)
下单购买



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**payment_service** | [**PaymentService**](PaymentService.md) |  | [required] |

### Return type

[**crate::models::PaymentOrder**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_delete_invoice

> bool payment_period_delete_invoice(app, id)
删除发票

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | 发票编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_delete_order

> bool payment_period_delete_order(app, ident)
取消订单

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 订单号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_invoices

> Vec<crate::models::PaymentInvoice> payment_period_invoices(app)
获取发票列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::PaymentInvoice>**](PaymentInvoice.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_order

> crate::models::PaymentOrder payment_period_order(app, ident)
获取订单详情

获取订单详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 订单号 | [required] |

### Return type

[**crate::models::PaymentOrder**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_orders

> Vec<crate::models::PaymentOrder> payment_period_orders(app)
订单列表

获取应用的订单列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::PaymentOrder>**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_orders_without_invoice

> Vec<crate::models::PaymentOrder> payment_period_orders_without_invoice(app)
获取未曾开票的订单列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::PaymentOrder>**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_price

> crate::models::PaymentService payment_period_price(app, service)
获取套餐价格



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**service** | [**PaymentService**](.md) | 配额信息 | [required] |

### Return type

[**crate::models::PaymentService**](PaymentService.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_receipt

> std::path::PathBuf payment_period_receipt(app, ident, id)
获取订单回执图片

查看回执图片

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 订单号 | [required] |
**id** | **i32** | 回执编号 | [required] |

### Return type

[**std::path::PathBuf**](std::path::PathBuf.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: default

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_request_contact

> bool payment_period_request_contact(app, contact_form)
联系销售获取私有化报价

获取私有化方案、报价等信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**contact_form** | [**ContactForm**](ContactForm.md) | 客户联系信息 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_upload_receipt

> crate::models::PaymentOrder payment_period_upload_receipt(app, ident, receipt)
上传转账回执

用户在完成银行转账后，通过该接口上传转账回执

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 订单号 | [required] |
**receipt** | Option<**std::path::PathBuf**> |  |  |

### Return type

[**crate::models::PaymentOrder**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## payment_period_wepay

> payment_period_wepay()
接受微信支付的支付结果

### Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


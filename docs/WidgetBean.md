# WidgetBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**ident** | Option<**String**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional][default to Web]
**sub_type** | Option<**String**> |  | [optional][default to None]
**intro** | Option<**String**> |  | [optional]
**queries** | Option<[**Vec<crate::models::QueryBean>**](QueryBean.md)> |  | [optional]
**layout** | Option<**String**> | 组件布局 | [optional]
**settings** | Option<[**serde_json::Value**](.md)> | 组件设置参数 | [optional]
**download_url** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



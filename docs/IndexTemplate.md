# IndexTemplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**intro** | Option<**String**> |  | [optional]
**mappings** | Option<[**serde_json::Value**](.md)> |  | [optional]
**query** | Option<[**serde_json::Value**](.md)> |  | [optional]
**widgets** | Option<[**Vec<serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**opened** | Option<**bool**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



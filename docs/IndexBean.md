# IndexBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**system** | Option<**bool**> | 是否系统索引（系统索引不允许用户删除） | [optional]
**intro** | Option<**String**> |  | [optional]
**alias** | Option<**Vec<String>**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**filters** | Option<[**Vec<serde_json::Value>**](serde_json::Value.md)> | 索引关联的临时过滤器 | [optional]
**aggs** | Option<[**serde_json::Value**](.md)> | 索引关联的临时聚合器 | [optional]
**sorts** | Option<[**Vec<serde_json::Value>**](serde_json::Value.md)> | 索引关联的临时排序 | [optional]
**shards** | Option<**i32**> | 该索引的分片数 | [optional]
**replicas** | Option<**i32**> | 该索引的副本数 | [optional]
**analyzer** | Option<**String**> |  | [optional]
**search_analyzer** | Option<**String**> |  | [optional]
**stat** | Option<[**crate::models::IndexStatBean**](IndexStatBean.md)> |  | [optional]
**options** | Option<[**serde_json::Value**](.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



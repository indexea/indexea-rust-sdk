# IndexTask

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**es_task_id** | Option<**String**> |  | [optional]
**es_detail** | Option<[**serde_json::Value**](.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**end_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



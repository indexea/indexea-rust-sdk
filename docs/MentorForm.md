# MentorForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | Option<**i32**> | 成员总数 | [optional]
**mentors** | Option<[**Vec<crate::models::AccountBean>**](AccountBean.md)> | 成员列表 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



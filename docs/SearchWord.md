# SearchWord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**q** | Option<**String**> | 搜索词 | [optional]
**count** | Option<**i32**> | 该词搜索次数 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



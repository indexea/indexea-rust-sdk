# \AppsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**app_period_add_mentor**](AppsApi.md#app_period_add_mentor) | **POST** /apps/{app}/mentors | 添加应用成员
[**app_period_blacklist**](AppsApi.md#app_period_blacklist) | **GET** /apps/{app}/blacklist | 获取黑名单信息
[**app_period_create**](AppsApi.md#app_period_create) | **POST** /apps | 创建应用
[**app_period_create_access_token**](AppsApi.md#app_period_create_access_token) | **POST** /apps/{app}/tokens | 创建 Access Token
[**app_period_create_oauth_app**](AppsApi.md#app_period_create_oauth_app) | **POST** /apps/{app}/oauth | 创建第三方应用
[**app_period_delete**](AppsApi.md#app_period_delete) | **DELETE** /apps/{app} | 删除应用
[**app_period_delete_access_token**](AppsApi.md#app_period_delete_access_token) | **DELETE** /apps/{app}/tokens | 删除 Access Token
[**app_period_delete_mentor**](AppsApi.md#app_period_delete_mentor) | **DELETE** /apps/{app}/mentors | 删除应用成员
[**app_period_delete_oauth_app**](AppsApi.md#app_period_delete_oauth_app) | **DELETE** /apps/{app}/oauth | 删除第三方应用
[**app_period_excel_of_logs**](AppsApi.md#app_period_excel_of_logs) | **GET** /apps/{app}/logs | 导出应用日志到 Excel
[**app_period_get**](AppsApi.md#app_period_get) | **GET** /apps/{app} | 获取应用详情
[**app_period_get_company**](AppsApi.md#app_period_get_company) | **POST** /apps/{app}/company | 获取应用填写的公司信息
[**app_period_get_company_pic**](AppsApi.md#app_period_get_company_pic) | **GET** /apps/{app}/company | 获取公司营业执照或者一般纳税人证明
[**app_period_list**](AppsApi.md#app_period_list) | **GET** /apps | 获取应用列表
[**app_period_list_mentors**](AppsApi.md#app_period_list_mentors) | **GET** /apps/{app}/mentors | 获取应用成员列表
[**app_period_list_oauth_apps**](AppsApi.md#app_period_list_oauth_apps) | **GET** /apps/{app}/oauth | 获取第三方应用列表
[**app_period_logs**](AppsApi.md#app_period_logs) | **POST** /apps/{app}/logs | 获取应用的日志列表
[**app_period_reset_access_token**](AppsApi.md#app_period_reset_access_token) | **PATCH** /apps/{app}/tokens | 重置 Access Token
[**app_period_reset_oauth_app_secret**](AppsApi.md#app_period_reset_oauth_app_secret) | **POST** /apps/{app}/oauth-reset-secret | 重新生成三方应用的密钥
[**app_period_save_blacklist**](AppsApi.md#app_period_save_blacklist) | **PUT** /apps/{app}/blacklist | 修改应用的黑名单信息
[**app_period_save_company**](AppsApi.md#app_period_save_company) | **PUT** /apps/{app}/company | 修改应用的公司信息
[**app_period_searchs_estimate**](AppsApi.md#app_period_searchs_estimate) | **GET** /apps/{app}/searchs-estimate | 获取搜索流量包使用配额信息
[**app_period_set_trigger**](AppsApi.md#app_period_set_trigger) | **PUT** /apps/{app}/trigger | 修改应用的触发器信息
[**app_period_tokens**](AppsApi.md#app_period_tokens) | **GET** /apps/{app}/tokens | 获取 Access Token 列表
[**app_period_transfer**](AppsApi.md#app_period_transfer) | **POST** /apps/{app}/transfer | 转让应用给他人
[**app_period_trigger**](AppsApi.md#app_period_trigger) | **GET** /apps/{app}/trigger | 获取应用触发器详情
[**app_period_trigger_logs**](AppsApi.md#app_period_trigger_logs) | **GET** /apps/{app}/trigger-logs | 获取应用触发日志列表
[**app_period_update**](AppsApi.md#app_period_update) | **PUT** /apps/{app} | 修改应用的基本信息
[**app_period_update_access_token**](AppsApi.md#app_period_update_access_token) | **PUT** /apps/{app}/tokens | 修改 Access Token
[**app_period_update_mentor**](AppsApi.md#app_period_update_mentor) | **PATCH** /apps/{app}/mentors | 修改成员备注和权限
[**app_period_update_mentor_options**](AppsApi.md#app_period_update_mentor_options) | **POST** /apps/{app}/mentors-options | 修改应用成员自身的设置（包括应用名备注，是否接收报告等）
[**app_period_update_mentor_report_options**](AppsApi.md#app_period_update_mentor_report_options) | **PATCH** /apps/{app}/mentors-options | 修改应用成员自身的通知设置
[**app_period_update_oauth_app**](AppsApi.md#app_period_update_oauth_app) | **PATCH** /apps/{app}/oauth | 修改第三方应用信息
[**app_period_update_oauth_app_logo**](AppsApi.md#app_period_update_oauth_app_logo) | **PUT** /apps/{app}/oauth | 修改三方应用图标
[**app_period_update_status**](AppsApi.md#app_period_update_status) | **PATCH** /apps/{app} | 修改应用的状态



## app_period_add_mentor

> crate::models::MentorForm app_period_add_mentor(app, account, scopes, name)
添加应用成员



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**account** | **String** | 成员账号 | [required] |
**scopes** | **String** | 权限 | [required] |
**name** | Option<**String**> | 备注名称 |  |

### Return type

[**crate::models::MentorForm**](MentorForm.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_blacklist

> crate::models::BlacklistBean app_period_blacklist(app)
获取黑名单信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**crate::models::BlacklistBean**](BlacklistBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_create

> crate::models::AppBean app_period_create(name, intro)
创建应用



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**name** | **String** |  | [required] |
**intro** | Option<**String**> |  |  |

### Return type

[**crate::models::AppBean**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_create_access_token

> crate::models::TokenBean app_period_create_access_token(app, token_bean)
创建 Access Token



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**token_bean** | [**TokenBean**](TokenBean.md) |  | [required] |

### Return type

[**crate::models::TokenBean**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_create_oauth_app

> crate::models::OauthAppBean app_period_create_oauth_app(app, oauth_app_bean)
创建第三方应用



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**oauth_app_bean** | [**OauthAppBean**](OauthAppBean.md) |  | [required] |

### Return type

[**crate::models::OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_delete

> bool app_period_delete(app)
删除应用



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_delete_access_token

> bool app_period_delete_access_token(app, id, vcode)
删除 Access Token



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | Access Token 编号 | [required] |
**vcode** | **String** |  | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_delete_mentor

> bool app_period_delete_mentor(app, account)
删除应用成员



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**account** | **i32** | 成员编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_delete_oauth_app

> bool app_period_delete_oauth_app(app, ident, vcode)
删除第三方应用



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 三方应用编号 | [required] |
**vcode** | **String** |  | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_excel_of_logs

> std::path::PathBuf app_period_excel_of_logs(app, account, indices, widget, query, r#type, start_date, end_date)
导出应用日志到 Excel

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**account** | Option<**i32**> | 账号 |  |[default to 0]
**indices** | Option<[**Vec<i32>**](i32.md)> | 索引 |  |
**widget** | Option<**i32**> | 组件 |  |[default to 0]
**query** | Option<**i32**> | 查询 |  |[default to 0]
**r#type** | Option<**i32**> | 类型 |  |[default to 0]
**start_date** | Option<**String**> | 起始日期 |  |
**end_date** | Option<**String**> | 结束日期 |  |

### Return type

[**std::path::PathBuf**](std::path::PathBuf.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_get

> crate::models::AppBean app_period_get(app)
获取应用详情



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**crate::models::AppBean**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_get_company

> crate::models::CompanyBean app_period_get_company(app)
获取应用填写的公司信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**crate::models::CompanyBean**](CompanyBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_get_company_pic

> std::path::PathBuf app_period_get_company_pic(app, r#type)
获取公司营业执照或者一般纳税人证明



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**r#type** | **String** | 获取图片的类型 [license,certificate] | [required] |

### Return type

[**std::path::PathBuf**](std::path::PathBuf.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: default

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_list

> Vec<crate::models::AppBean> app_period_list()
获取应用列表



### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<crate::models::AppBean>**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_list_mentors

> crate::models::MentorForm app_period_list_mentors(app, from, size)
获取应用成员列表



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**from** | Option<**i32**> | 开始位置 |  |[default to 0]
**size** | Option<**i32**> | 获取的数量 |  |[default to 20]

### Return type

[**crate::models::MentorForm**](MentorForm.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_list_oauth_apps

> Vec<crate::models::OauthAppBean> app_period_list_oauth_apps(app)
获取第三方应用列表



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::OauthAppBean>**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_logs

> crate::models::AppLogsBean app_period_logs(app, account, indices, widget, query, r#type, start_date, end_date, from, size)
获取应用的日志列表



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**account** | Option<**i32**> | 账号 |  |[default to 0]
**indices** | Option<[**Vec<i32>**](i32.md)> | 索引 |  |
**widget** | Option<**i32**> | 组件 |  |[default to 0]
**query** | Option<**i32**> | 查询 |  |[default to 0]
**r#type** | Option<**i32**> | 类型 |  |[default to 0]
**start_date** | Option<**String**> | 起始日期 |  |
**end_date** | Option<**String**> | 结束日期 |  |
**from** | Option<**i32**> | 起始位置 |  |[default to 0]
**size** | Option<**i32**> | 数量 |  |[default to 20]

### Return type

[**crate::models::AppLogsBean**](AppLogsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_reset_access_token

> crate::models::TokenBean app_period_reset_access_token(app, id, vcode)
重置 Access Token



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | Access Token 编号 | [required] |
**vcode** | **String** |  | [required] |

### Return type

[**crate::models::TokenBean**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_reset_oauth_app_secret

> crate::models::OauthAppBean app_period_reset_oauth_app_secret(app, ident, vcode)
重新生成三方应用的密钥



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 三方应用标识 | [required] |
**vcode** | **String** |  | [required] |

### Return type

[**crate::models::OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_save_blacklist

> bool app_period_save_blacklist(app, blacklist_bean)
修改应用的黑名单信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**blacklist_bean** | [**BlacklistBean**](BlacklistBean.md) |  | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_save_company

> crate::models::CompanyBean app_period_save_company(app, name, url, nation, province, city, taxpayer, bank, account, address, tel, license, certificate, post_addr, post_code, post_name, post_tel)
修改应用的公司信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**name** | Option<**String**> | 公司名称 |  |
**url** | Option<**String**> | 公司网址 |  |
**nation** | Option<**String**> | 国家 |  |
**province** | Option<**String**> | 省/州/地区 |  |
**city** | Option<**String**> | 城市 |  |
**taxpayer** | Option<**String**> | 纳税人识别号 |  |
**bank** | Option<**String**> | 开户行 |  |
**account** | Option<**String**> | 银行账号 |  |
**address** | Option<**String**> | 公司注册地址 |  |
**tel** | Option<**String**> | 公司电话 |  |
**license** | Option<**std::path::PathBuf**> | 营业执照 |  |
**certificate** | Option<**std::path::PathBuf**> | 一般纳税人证明扫描件 |  |
**post_addr** | Option<**String**> | 快递地址 |  |
**post_code** | Option<**String**> | 邮编 |  |
**post_name** | Option<**String**> | 收件人姓名 |  |
**post_tel** | Option<**String**> | 收件人电话 |  |

### Return type

[**crate::models::CompanyBean**](CompanyBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_searchs_estimate

> crate::models::SearchEstimateResult app_period_searchs_estimate(app, days)
获取搜索流量包使用配额信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**days** | **i32** | 计算平均搜索数的最近天数 | [required] |[default to 30]

### Return type

[**crate::models::SearchEstimateResult**](SearchEstimateResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_set_trigger

> bool app_period_set_trigger(app, trigger_bean)
修改应用的触发器信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**trigger_bean** | [**TriggerBean**](TriggerBean.md) |  | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_tokens

> Vec<crate::models::TokenBean> app_period_tokens(app)
获取 Access Token 列表



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::TokenBean>**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_transfer

> bool app_period_transfer(app, vcode, account)
转让应用给他人



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**vcode** | **String** | 验证码 | [required] |
**account** | **i32** | 目标账号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_trigger

> crate::models::TriggerBean app_period_trigger(app)
获取应用触发器详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**crate::models::TriggerBean**](TriggerBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_trigger_logs

> Vec<crate::models::TriggerLogBean> app_period_trigger_logs(app, id, size)
获取应用触发日志列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | 起始日志编号 | [required] |[default to 999999999]
**size** | **i32** | 日志数 | [required] |[default to 20]

### Return type

[**Vec<crate::models::TriggerLogBean>**](TriggerLogBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update

> crate::models::AppBean app_period_update(app, name, intro)
修改应用的基本信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**name** | Option<**String**> |  |  |
**intro** | Option<**String**> |  |  |

### Return type

[**crate::models::AppBean**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update_access_token

> crate::models::TokenBean app_period_update_access_token(app, token_bean)
修改 Access Token



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**token_bean** | [**TokenBean**](TokenBean.md) |  | [required] |

### Return type

[**crate::models::TokenBean**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update_mentor

> bool app_period_update_mentor(app, account, name, scopes)
修改成员备注和权限



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**account** | **i32** | 成员编号 | [required] |
**name** | **String** | 备注名称 | [required] |
**scopes** | **String** | 权限 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update_mentor_options

> bool app_period_update_mentor_options(app, name, report)
修改应用成员自身的设置（包括应用名备注，是否接收报告等）



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**name** | Option<**String**> | 应用备注名称，如果不填写则使用默认名称 |  |
**report** | Option<**bool**> | 是否接收使用情况报告 |  |[default to false]

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update_mentor_report_options

> bool app_period_update_mentor_report_options(app, key, r#type, value, vcode)
修改应用成员自身的通知设置



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**key** | **String** | 配置项名称 | [required] |
**r#type** | **String** | 配置值类型 | [required] |
**value** | **String** | 配置值 | [required] |
**vcode** | Option<**String**> | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) |  |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update_oauth_app

> crate::models::OauthAppBean app_period_update_oauth_app(app, oauth_app_bean)
修改第三方应用信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**oauth_app_bean** | [**OauthAppBean**](OauthAppBean.md) |  | [required] |

### Return type

[**crate::models::OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update_oauth_app_logo

> crate::models::OauthAppBean app_period_update_oauth_app_logo(app, ident, logo)
修改三方应用图标



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**ident** | **String** | 三方应用标识 | [required] |
**logo** | Option<**std::path::PathBuf**> |  |  |

### Return type

[**crate::models::OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## app_period_update_status

> bool app_period_update_status(app, vcode, status)
修改应用的状态



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**vcode** | **String** | 验证码 | [required] |
**status** | **i32** | 新状态 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


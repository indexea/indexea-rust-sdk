# QueryActionBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**query** | Option<**i32**> |  | [optional]
**widget** | Option<**i32**> |  | [optional]
**recomm** | Option<**i32**> |  | [optional]
**error** | Option<[**serde_json::Value**](.md)> |  | [optional]
**user** | Option<**String**> |  | [optional]
**q** | Option<**Vec<String>**> |  | [optional]
**referer** | Option<**String**> |  | [optional]
**params** | Option<[**serde_json::Value**](.md)> |  | [optional]
**hits** | Option<**i32**> |  | [optional]
**took** | Option<**i32**> |  | [optional]
**ip** | Option<**String**> |  | [optional]
**nation** | Option<**String**> |  | [optional]
**province** | Option<**String**> |  | [optional]
**city** | Option<**String**> |  | [optional]
**browser** | Option<**String**> |  | [optional]
**os** | Option<**String**> |  | [optional]
**clicks** | Option<**Vec<String>**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



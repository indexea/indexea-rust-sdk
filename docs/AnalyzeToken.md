# AnalyzeToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | Option<**String**> |  | [optional]
**start_offset** | Option<**f32**> |  | [optional]
**end_offset** | Option<**f32**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**position** | Option<**f32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# PaymentOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**r#type** | Option<**i32**> | 订单类型：1新购订单,2存续费订单,3纯扩容订单，4续费+扩容订单 | [optional]
**account** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**ident** | Option<**String**> | 订单号 | [optional]
**storage** | Option<**i32**> | 存储空间 | [optional]
**shards** | Option<**i32**> | 分片数 | [optional]
**replicas** | Option<**i32**> | 副本数 | [optional]
**searchs** | Option<**i32**> | 搜索次数 | [optional]
**days** | Option<**i32**> | 购买的天数 | [optional]
**price** | Option<**i32**> |  | [optional]
**payment** | Option<**String**> |  | [optional]
**remark** | Option<**String**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**expired_at** | Option<**String**> |  | [optional]
**receipt** | Option<**i32**> | 订单的银行回执 | [optional]
**receipt_url** | Option<**String**> | 银行回执的图片地址 | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



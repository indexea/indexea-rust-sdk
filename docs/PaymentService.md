# PaymentService

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days** | Option<**i32**> | 服务天数 | [optional]
**storage** | Option<**i32**> | 存储空间 | [optional]
**shards** | Option<**i32**> | 分片数 | [optional]
**replicas** | Option<**i32**> | 副本数 | [optional]
**searchs** | Option<**i32**> | 搜索次数 | [optional]
**price** | Option<**i32**> | 服务价格 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



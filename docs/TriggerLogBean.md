# TriggerLogBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**log** | Option<**i32**> |  | [optional]
**url** | Option<**String**> |  | [optional]
**body** | Option<**String**> |  | [optional]
**resp_code** | Option<**i32**> |  | [optional]
**resp_body** | Option<**String**> |  | [optional]
**resp_time** | Option<**i32**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TokenBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**token** | Option<**String**> |  | [optional]
**scopes** | Option<[**serde_json::Value**](.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**expired_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# OpenidBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> | 类型 | [optional]
**openid** | Option<**String**> | 三方账号唯一标识 | [optional]
**name** | Option<**String**> | 三方账号昵称 | [optional]
**created_at** | Option<**String**> | 首次登录时间 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \IndicesApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**index_period_cleanup**](IndicesApi.md#index_period_cleanup) | **POST** /indices/{app}/{index}/cleanup | 清空索引记录
[**index_period_copy_to**](IndicesApi.md#index_period_copy_to) | **POST** /indices/{app}/{index}/copyto | 导出索引数据
[**index_period_create**](IndicesApi.md#index_period_create) | **POST** /indices/{app} | 创建索引
[**index_period_create_template**](IndicesApi.md#index_period_create_template) | **POST** /indices/templates | 创建索引模板
[**index_period_delete**](IndicesApi.md#index_period_delete) | **DELETE** /indices/{app}/{index} | 删除索引
[**index_period_delete_crawler_task**](IndicesApi.md#index_period_delete_crawler_task) | **DELETE** /indices/{app}/{index}/crawler-settings | 删除索引的数据爬取任务
[**index_period_delete_template**](IndicesApi.md#index_period_delete_template) | **DELETE** /indices/templates | 删除索引模板
[**index_period_export**](IndicesApi.md#index_period_export) | **POST** /indices/{app}/{index}/export | 导出索引数据
[**index_period_flush**](IndicesApi.md#index_period_flush) | **POST** /indices/{app}/{index}/flush | 清空索引记录
[**index_period_flush_settings**](IndicesApi.md#index_period_flush_settings) | **PUT** /indices/{app}/{index}/settings | 写入设置信息到索引
[**index_period_get**](IndicesApi.md#index_period_get) | **GET** /indices/{app}/{index} | 获取单个索引详情
[**index_period_get_crawler_logs**](IndicesApi.md#index_period_get_crawler_logs) | **GET** /indices/{app}/{index}/crawler-logs | 获取索引的爬虫任务的爬取日志
[**index_period_get_crawler_task**](IndicesApi.md#index_period_get_crawler_task) | **GET** /indices/{app}/{index}/crawler-settings | 获取索引的爬虫任务设定
[**index_period_get_filter_settings**](IndicesApi.md#index_period_get_filter_settings) | **GET** /indices/{app}/{index}/filter-settings | 获取索引设置信息
[**index_period_get_settings**](IndicesApi.md#index_period_get_settings) | **GET** /indices/{app}/{index}/settings | 获取索引设置信息
[**index_period_list**](IndicesApi.md#index_period_list) | **GET** /indices/{app} | 获取应用的索引列表
[**index_period_list_templates**](IndicesApi.md#index_period_list_templates) | **GET** /indices/templates | 获取所有可用的索引模板
[**index_period_prefetch**](IndicesApi.md#index_period_prefetch) | **GET** /indices/crawler | 获取目标网站内容预览
[**index_period_rebuild**](IndicesApi.md#index_period_rebuild) | **POST** /indices/{app}/{index}/rebuild | 重建索引数据
[**index_period_rebuild_task**](IndicesApi.md#index_period_rebuild_task) | **GET** /indices/{app}/{index}/rebuild | 获取重建索引任务的详情
[**index_period_submit_crawler_task**](IndicesApi.md#index_period_submit_crawler_task) | **POST** /indices/{app}/{index}/crawler-settings | 提交或者更新索引的数据爬取任务
[**index_period_update**](IndicesApi.md#index_period_update) | **PUT** /indices/{app}/{index} | 修改索引
[**index_period_update_settings**](IndicesApi.md#index_period_update_settings) | **POST** /indices/{app}/{index}/settings | 更新索引设置信息
[**index_period_update_template**](IndicesApi.md#index_period_update_template) | **PUT** /indices/templates | 修改索引模板
[**synonyms_period_add**](IndicesApi.md#synonyms_period_add) | **POST** /indices/{app}/{index}/synonyms | 添加同义词
[**synonyms_period_delete**](IndicesApi.md#synonyms_period_delete) | **DELETE** /indices/{app}/{index}/synonyms | 删除同义词
[**synonyms_period_enable**](IndicesApi.md#synonyms_period_enable) | **PATCH** /indices/{app}/{index}/synonyms | 启用禁用同义词
[**synonyms_period_flush**](IndicesApi.md#synonyms_period_flush) | **POST** /indices/{app}/{index}/synonyms-flush | 将同义词更新到搜索引擎的同义词表
[**synonyms_period_import**](IndicesApi.md#synonyms_period_import) | **POST** /indices/{app}/{index}/synonyms-import | 导入同义词
[**synonyms_period_list**](IndicesApi.md#synonyms_period_list) | **GET** /indices/{app}/{index}/synonyms | 获取索引的所有同义词
[**synonyms_period_update**](IndicesApi.md#synonyms_period_update) | **PUT** /indices/{app}/{index}/synonyms | 修改同义词



## index_period_cleanup

> serde_json::Value index_period_cleanup(app, index, vcode)
清空索引记录

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**vcode** | **String** | 验证码 | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_copy_to

> crate::models::IndexBean index_period_copy_to(app, index, name, fields, queries, records)
导出索引数据

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**name** | **String** | 新索引的名称 | [required] |
**fields** | **bool** | 是否复制字段定义 | [required] |
**queries** | **bool** | 是否复制所有查询 | [required] |
**records** | **bool** | 是否复制所有文档 | [required] |

### Return type

[**crate::models::IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_create

> crate::models::IndexBean index_period_create(app, index_form)
创建索引

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index_form** | [**IndexForm**](IndexForm.md) |  | [required] |

### Return type

[**crate::models::IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_create_template

> crate::models::IndexTemplate index_period_create_template(index_template)
创建索引模板

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**index_template** | [**IndexTemplate**](IndexTemplate.md) |  | [required] |

### Return type

[**crate::models::IndexTemplate**](IndexTemplate.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_delete

> bool index_period_delete(app, index, vcode)
删除索引

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**vcode** | **String** |  | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_delete_crawler_task

> bool index_period_delete_crawler_task(app, index)
删除索引的数据爬取任务

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_delete_template

> bool index_period_delete_template(id)
删除索引模板

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **i32** |  | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_export

> bool index_period_export(app, index, format)
导出索引数据

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**format** | **String** | 导出数据的格式 | [required] |[default to json]

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_flush

> bool index_period_flush(app, index)
清空索引记录

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_flush_settings

> serde_json::Value index_period_flush_settings(app, index, r#type, password)
写入设置信息到索引

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**r#type** | **String** | 设置类型 | [required] |
**password** | **String** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_get

> crate::models::IndexBean index_period_get(app, index)
获取单个索引详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |

### Return type

[**crate::models::IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_get_crawler_logs

> crate::models::CrawlerLogs index_period_get_crawler_logs(app, index, from, size)
获取索引的爬虫任务的爬取日志

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**from** | **i32** |  | [required] |
**size** | **i32** |  | [required] |

### Return type

[**crate::models::CrawlerLogs**](CrawlerLogs.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_get_crawler_task

> crate::models::CrawlerTask index_period_get_crawler_task(app, index)
获取索引的爬虫任务设定

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |

### Return type

[**crate::models::CrawlerTask**](CrawlerTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_get_filter_settings

> Vec<String> index_period_get_filter_settings(app, index, r#type)
获取索引设置信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**r#type** | **String** | 设置类型 | [required] |

### Return type

**Vec<String>**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_get_settings

> crate::models::IndexSettings index_period_get_settings(app, index, r#type)
获取索引设置信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**r#type** | **String** | 设置类型 | [required] |

### Return type

[**crate::models::IndexSettings**](IndexSettings.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_list

> Vec<crate::models::IndexBean> index_period_list(app, stat)
获取应用的索引列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**stat** | Option<**bool**> | 是否包含索引的统计信息 |  |[default to true]

### Return type

[**Vec<crate::models::IndexBean>**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_list_templates

> crate::models::IndexTemplates index_period_list_templates(from, size)
获取所有可用的索引模板

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**from** | **i32** |  | [required] |[default to 0]
**size** | **i32** |  | [required] |[default to 100]

### Return type

[**crate::models::IndexTemplates**](IndexTemplates.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_prefetch

> Vec<serde_json::Value> index_period_prefetch(r#type, url)
获取目标网站内容预览

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**r#type** | **String** |  | [required] |
**url** | **String** |  | [required] |

### Return type

[**Vec<serde_json::Value>**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_rebuild

> crate::models::IndexTask index_period_rebuild(app, index, index_rebuild_form)
重建索引数据

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**index_rebuild_form** | [**IndexRebuildForm**](IndexRebuildForm.md) |  | [required] |

### Return type

[**crate::models::IndexTask**](IndexTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_rebuild_task

> crate::models::IndexTask index_period_rebuild_task(app, index, task_id)
获取重建索引任务的详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**task_id** | **i32** | 任务编号,传0则获取最新的任务信息 | [required] |[default to 0]

### Return type

[**crate::models::IndexTask**](IndexTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_submit_crawler_task

> crate::models::CrawlerTask index_period_submit_crawler_task(app, index, crawler_task)
提交或者更新索引的数据爬取任务

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**crawler_task** | [**CrawlerTask**](CrawlerTask.md) |  | [required] |

### Return type

[**crate::models::CrawlerTask**](CrawlerTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_update

> crate::models::IndexBean index_period_update(app, index, index_form)
修改索引

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**index_form** | [**IndexForm**](IndexForm.md) |  | [required] |

### Return type

[**crate::models::IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_update_settings

> bool index_period_update_settings(app, index, r#type, body)
更新索引设置信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**r#type** | **String** | 设置类型 | [required] |
**body** | Option<**String**> |  |  |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: text/plain
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## index_period_update_template

> crate::models::IndexTemplate index_period_update_template(index_template)
修改索引模板

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**index_template** | [**IndexTemplate**](IndexTemplate.md) |  | [required] |

### Return type

[**crate::models::IndexTemplate**](IndexTemplate.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## synonyms_period_add

> crate::models::SynonymsBean synonyms_period_add(app, index, synonyms_bean)
添加同义词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**synonyms_bean** | [**SynonymsBean**](SynonymsBean.md) |  | [required] |

### Return type

[**crate::models::SynonymsBean**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## synonyms_period_delete

> bool synonyms_period_delete(app, index, id)
删除同义词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**id** | **i32** | 同义词编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## synonyms_period_enable

> bool synonyms_period_enable(app, index, id, enable, all)
启用禁用同义词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**id** | **i32** | 同义词编号 | [required] |
**enable** | Option<**bool**> | 是否启用 |  |
**all** | Option<**bool**> | 是否对所有索引起作用 |  |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## synonyms_period_flush

> serde_json::Value synonyms_period_flush(app, index, password)
将同义词更新到搜索引擎的同义词表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**password** | **String** |  | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## synonyms_period_import

> Vec<crate::models::SynonymsBean> synonyms_period_import(app, index, action, synonyms_bean)
导入同义词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**action** | **i32** | 覆盖方式 | [required] |
**synonyms_bean** | [**Vec<crate::models::SynonymsBean>**](SynonymsBean.md) |  | [required] |

### Return type

[**Vec<crate::models::SynonymsBean>**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## synonyms_period_list

> Vec<crate::models::SynonymsBean> synonyms_period_list(app, index, size, r#type, from)
获取索引的所有同义词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**size** | **i32** |  | [required] |[default to 99999]
**r#type** | Option<**i32**> | 类型 |  |[default to 0]
**from** | Option<**i32**> |  |  |[default to 0]

### Return type

[**Vec<crate::models::SynonymsBean>**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## synonyms_period_update

> crate::models::SynonymsBean synonyms_period_update(app, index, synonyms_bean)
修改同义词

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**synonyms_bean** | [**SynonymsBean**](SynonymsBean.md) |  | [required] |

### Return type

[**crate::models::SynonymsBean**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


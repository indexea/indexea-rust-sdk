# Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**sender** | Option<**i32**> |  | [optional]
**receiver** | Option<**i32**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**avater** | Option<**String**> |  | [optional]
**msg** | Option<**String**> |  | [optional]
**r#type** | Option<**i32**> |  | [optional]
**msgid** | Option<**i32**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**read_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



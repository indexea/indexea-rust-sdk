# \AccountApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**account_period_bulletin**](AccountApi.md#account_period_bulletin) | **GET** /accounts/bulletin | 获取系统公告
[**account_period_delete**](AccountApi.md#account_period_delete) | **DELETE** /account/profile | 注销账号
[**account_period_delete_openid**](AccountApi.md#account_period_delete_openid) | **DELETE** /account/openid | 解绑三方账号
[**account_period_openid**](AccountApi.md#account_period_openid) | **GET** /account/openid | 获取绑定的所有三方账号
[**account_period_passwd**](AccountApi.md#account_period_passwd) | **POST** /account/passwd | 修改账号密码
[**account_period_portrait**](AccountApi.md#account_period_portrait) | **POST** /account/portrait | 修改账号头像
[**account_period_profile**](AccountApi.md#account_period_profile) | **GET** /account/profile | 获取登录账号信息
[**account_period_reset_pwd**](AccountApi.md#account_period_reset_pwd) | **POST** /account/reset-pwd | 重置账号密码
[**account_period_send_verify_code**](AccountApi.md#account_period_send_verify_code) | **GET** /account/send-verify-code | 发送账号验证码
[**account_period_signin**](AccountApi.md#account_period_signin) | **POST** /account/signin | 登录系统
[**account_period_signout**](AccountApi.md#account_period_signout) | **POST** /account/sign-out | 退出登录状态
[**account_period_signup**](AccountApi.md#account_period_signup) | **POST** /account/signup | 注册新账号
[**account_period_update**](AccountApi.md#account_period_update) | **POST** /account/profile | 修改账号资料
[**account_period_update_settings**](AccountApi.md#account_period_update_settings) | **POST** /account/settings | 修改账号设置



## account_period_bulletin

> crate::models::Bulletin account_period_bulletin()
获取系统公告

获取系统公告

### Parameters

This endpoint does not need any parameter.

### Return type

[**crate::models::Bulletin**](Bulletin.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_delete

> bool account_period_delete(pwd, vcode)
注销账号



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**pwd** | **String** | 操作者的登录密码 | [required] |
**vcode** | **String** | 操作验证码 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_delete_openid

> bool account_period_delete_openid(r#type, openid)
解绑三方账号



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**r#type** | **String** | 三方账号类型 | [required] |
**openid** | **String** | 三方账号唯一标识 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_openid

> Vec<crate::models::OpenidBean> account_period_openid()
获取绑定的所有三方账号



### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<crate::models::OpenidBean>**](OpenidBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_passwd

> bool account_period_passwd(password, new_password)
修改账号密码



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**password** | **String** | 旧密码 | [required] |
**new_password** | **String** | 新密码 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_portrait

> crate::models::AccountBean account_period_portrait(portrait)
修改账号头像



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**portrait** | Option<**std::path::PathBuf**> |  |  |

### Return type

[**crate::models::AccountBean**](AccountBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_profile

> crate::models::AccountBean account_period_profile(account)
获取登录账号信息



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account** | Option<**String**> | 获取指定账号信息，如果不指定，则返回当前登录账号 |  |

### Return type

[**crate::models::AccountBean**](AccountBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_reset_pwd

> bool account_period_reset_pwd(account, verify_code, pwd)
重置账号密码



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account** | **String** |  | [required] |
**verify_code** | **String** |  | [required] |
**pwd** | **String** |  | [required] |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_send_verify_code

> bool account_period_send_verify_code(account, purpose)
发送账号验证码



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account** | **String** | 账号 | [required] |
**purpose** | **String** | 验证码的用途 | [required] |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_signin

> crate::models::AccountBean account_period_signin(account, pwd, keep_login)
登录系统



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account** | **String** |  | [required] |
**pwd** | **String** |  | [required] |
**keep_login** | Option<**bool**> |  |  |[default to false]

### Return type

[**crate::models::AccountBean**](AccountBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_signout

> account_period_signout()
退出登录状态



### Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_signup

> crate::models::AccountBean account_period_signup(account, pwd, name, activate_code)
注册新账号



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account** | **String** |  | [required] |
**pwd** | **String** |  | [required] |
**name** | **String** |  | [required] |
**activate_code** | **String** |  | [required] |

### Return type

[**crate::models::AccountBean**](AccountBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_update

> crate::models::AccountBean account_period_update(account_bean)
修改账号资料



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_bean** | [**AccountBean**](AccountBean.md) |  | [required] |

### Return type

[**crate::models::AccountBean**](AccountBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_period_update_settings

> bool account_period_update_settings(key, r#type, value, vcode)
修改账号设置



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**key** | **String** | 配置项名称 | [required] |
**r#type** | **String** | 配置值类型 | [required] |
**value** | **String** | 配置值 | [required] |
**vcode** | Option<**String**> | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) |  |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


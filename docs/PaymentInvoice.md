# PaymentInvoice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**orders** | Option<[**Vec<crate::models::PaymentOrder>**](PaymentOrder.md)> |  | [optional]
**price** | Option<**i32**> |  | [optional]
**r#type** | Option<**i32**> |  | [optional]
**status** | Option<**i32**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**logs** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



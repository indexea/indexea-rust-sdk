# RecommendBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> | 编号 | [optional]
**ident** | Option<**String**> | 推荐标识 | [optional]
**app** | Option<**i32**> | 应用编号 | [optional]
**account** | Option<**i32**> | 账号 | [optional]
**index** | Option<**i32**> | 关联的索引编号 | [optional]
**name** | Option<**String**> | 名称 | [optional]
**intro** | Option<**String**> | 简介 | [optional]
**r#type** | Option<**String**> | 类型 | [optional]
**settings** | Option<[**serde_json::Value**](.md)> | 推荐设定 | [optional]
**status** | Option<**i32**> | 状态 | [optional]
**created_at** | Option<**String**> | 创建时间 | [optional]
**updated_at** | Option<**String**> | 最后更新时间 | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \RecommendApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**recommend_period_click**](RecommendApi.md#recommend_period_click) | **POST** /recommend/{ident}/click | 推荐结果点击行为收集
[**recommend_period_create**](RecommendApi.md#recommend_period_create) | **POST** /recommends/{app} | 创建新的推荐
[**recommend_period_delete**](RecommendApi.md#recommend_period_delete) | **DELETE** /recommends/{app} | 删除推荐
[**recommend_period_detail**](RecommendApi.md#recommend_period_detail) | **GET** /recommend/{ident} | 获取推荐的记录列表
[**recommend_period_fetch**](RecommendApi.md#recommend_period_fetch) | **POST** /recommend/{ident} | 获取推荐的记录列表
[**recommend_period_list**](RecommendApi.md#recommend_period_list) | **GET** /recommends/{app} | 获取已定义的推荐列表
[**recommend_period_update**](RecommendApi.md#recommend_period_update) | **PUT** /recommends/{app} | 更新推荐信息



## recommend_period_click

> bool recommend_period_click(ident, action_id, doc_id, userid, x_token)
推荐结果点击行为收集

该接口主要用于记录用户对推荐结果的点击行为

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ident** | **String** | 推荐的唯一标识 | [required] |
**action_id** | **String** | 对应推荐行为编号 | [required] |
**doc_id** | **String** | 对应索引中的内部记录编号 | [required] |
**userid** | Option<**String**> | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## recommend_period_create

> crate::models::RecommendBean recommend_period_create(app, recommend_bean)
创建新的推荐

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**recommend_bean** | [**RecommendBean**](RecommendBean.md) | 推荐信息 | [required] |

### Return type

[**crate::models::RecommendBean**](RecommendBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## recommend_period_delete

> bool recommend_period_delete(app, id)
删除推荐

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**id** | **i32** | 推荐编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## recommend_period_detail

> crate::models::RecommendBean recommend_period_detail(ident, x_token)
获取推荐的记录列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ident** | **String** | 推荐定义的标识 | [required] |
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |

### Return type

[**crate::models::RecommendBean**](RecommendBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## recommend_period_fetch

> serde_json::Value recommend_period_fetch(ident, x_token, userid, condition, from, count)
获取推荐的记录列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ident** | **String** | 推荐定义的标识 | [required] |
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |
**userid** | Option<**String**> | 访客的唯一标识，该标识由搜索前端生成，长度不超过64 |  |
**condition** | Option<[**::std::collections::HashMap<String, String>**](String.md)> | 获取某个记录的参数,例如 id=11223(后端将使用 term query 进行匹配) |  |
**from** | Option<**i32**> | 起始值 |  |[default to 0]
**count** | Option<**i32**> | 推荐的记录数 |  |[default to 10]

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## recommend_period_list

> Vec<crate::models::RecommendBean> recommend_period_list(app)
获取已定义的推荐列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::RecommendBean>**](RecommendBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## recommend_period_update

> bool recommend_period_update(app, recommend_bean)
更新推荐信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**recommend_bean** | [**RecommendBean**](RecommendBean.md) | 推荐信息 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


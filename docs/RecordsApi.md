# \RecordsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**record_period_delete**](RecordsApi.md#record_period_delete) | **DELETE** /records/{app}/{index} | 删除记录数据
[**record_period_get**](RecordsApi.md#record_period_get) | **GET** /records/{app}/{index} | 获取单条记录详情
[**record_period_list**](RecordsApi.md#record_period_list) | **POST** /records/{app}/{index} | 获取索引记录列表
[**record_period_push**](RecordsApi.md#record_period_push) | **PUT** /records/{app}/{index} | 插入或者更新索引数据
[**record_period_upload**](RecordsApi.md#record_period_upload) | **POST** /records/{app}/{index}/upload | 上传记录



## record_period_delete

> bool record_period_delete(app, index, _id)
删除记录数据

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**_id** | [**Vec<String>**](String.md) | 主键字段值 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## record_period_get

> serde_json::Value record_period_get(app, index, _id)
获取单条记录详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**_id** | **String** | 记录 _id 值 | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## record_period_list

> Vec<serde_json::Value> record_period_list(app, index, q, field, from, size, save_filter, record_filter)
获取索引记录列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**q** | Option<**String**> | 搜索关键字 |  |
**field** | Option<**String**> | 搜索字段 |  |
**from** | Option<**i32**> | 起始记录 |  |[default to 0]
**size** | Option<**i32**> | 获取记录数 |  |[default to 20]
**save_filter** | Option<**bool**> | 是否保存过滤器信息 |  |[default to false]
**record_filter** | Option<[**RecordFilter**](RecordFilter.md)> |  |  |

### Return type

[**Vec<serde_json::Value>**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## record_period_push

> bool record_period_push(app, index, request_body, combine)
插入或者更新索引数据

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**request_body** | [**Vec<serde_json::Value>**](serde_json::Value.md) |  | [required] |
**combine** | Option<**bool**> | 更新策略：合并还是替换，combine=true 为合并模式 |  |[default to false]

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## record_period_upload

> serde_json::Value record_period_upload(app, index, combine, use_id_as_id_value, files)
上传记录

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | **i32** | 索引编号 | [required] |
**combine** | Option<**bool**> | 更新策略：合并还是替换，combine=true 为合并模式 |  |[default to false]
**use_id_as_id_value** | Option<**bool**> | 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成 |  |[default to true]
**files** | Option<[**Vec<std::path::PathBuf>**](std::path::PathBuf.md)> |  |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


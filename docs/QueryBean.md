# QueryBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**ident** | Option<**String**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**indices** | Option<[**Vec<crate::models::IndexBean>**](IndexBean.md)> | 关联的索引列表 | [optional]
**name** | Option<**String**> |  | [optional]
**intro** | Option<**String**> |  | [optional]
**query** | Option<[**serde_json::Value**](.md)> |  | [optional]
**suggest** | Option<[**serde_json::Value**](.md)> |  | [optional]
**script_score** | Option<[**serde_json::Value**](.md)> |  | [optional]
**script_fields** | Option<[**serde_json::Value**](.md)> |  | [optional]
**sort_fields** | Option<[**Vec<crate::models::QuerySortField>**](QuerySortField.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**settings** | Option<[**serde_json::Value**](.md)> | 查询设置项 | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



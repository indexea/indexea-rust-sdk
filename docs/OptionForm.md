# OptionForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | 配置项名称 | 
**r#type** | **String** | 配置值类型 | 
**value** | **String** | 配置值 | 
**vcode** | Option<**String**> | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



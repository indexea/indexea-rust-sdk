# QueryForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | Option<**String**> |  | [optional]
**intro** | Option<**String**> |  | [optional]
**indices** | Option<**Vec<i32>**> | 关联的索引列表 | [optional]
**query** | Option<[**serde_json::Value**](.md)> | 查询条件 | [optional]
**suggest** | Option<[**serde_json::Value**](.md)> |  | [optional]
**script_score** | Option<[**serde_json::Value**](.md)> |  | [optional]
**script_fields** | Option<[**serde_json::Value**](.md)> |  | [optional]
**sort_fields** | Option<[**Vec<crate::models::QuerySortField>**](QuerySortField.md)> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



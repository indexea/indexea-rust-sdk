# \StatsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stats_period_recomms**](StatsApi.md#stats_period_recomms) | **GET** /stats/{app}/recomms | 获取推荐日志的汇总信息
[**stats_period_searchs**](StatsApi.md#stats_period_searchs) | **GET** /stats/{app}/searchs | 获取搜索日志的汇总信息
[**stats_period_top_clicks**](StatsApi.md#stats_period_top_clicks) | **GET** /stats/{app}/top-clicks | 获取点击排行榜
[**stats_period_widgets**](StatsApi.md#stats_period_widgets) | **GET** /stats/{app}/widgets | 获取模板与组件的统计信息



## stats_period_recomms

> serde_json::Value stats_period_recomms(app, recomm, start_date, end_date, interval)
获取推荐日志的汇总信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**recomm** | Option<**i32**> | 统计指定推荐 |  |[default to 0]
**start_date** | Option<**String**> | 统计起始日期 |  |
**end_date** | Option<**String**> | 统计结束日期 |  |
**interval** | Option<**String**> | 统计间隔 - 日、周、月、季度、年 |  |[default to date]

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## stats_period_searchs

> serde_json::Value stats_period_searchs(app, index, query, widget, start_date, end_date, interval)
获取搜索日志的汇总信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**index** | Option<**i32**> | 统计指定索引 |  |[default to 0]
**query** | Option<**i32**> | 统计指定查询 |  |[default to 0]
**widget** | Option<**i32**> | 统计指定组件 |  |[default to 0]
**start_date** | Option<**String**> | 统计起始日期 |  |
**end_date** | Option<**String**> | 统计结束日期 |  |
**interval** | Option<**String**> | 统计间隔 - 日、周、月、季度、年 |  |[default to date]

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## stats_period_top_clicks

> Vec<serde_json::Value> stats_period_top_clicks(app, size, index, query, recomm, widget, start_date, end_date)
获取点击排行榜

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**size** | **i32** | 排行榜大小 | [required] |[default to 10]
**index** | Option<**i32**> | 统计指定索引 |  |[default to 0]
**query** | Option<**i32**> | 统计指定查询 |  |[default to 0]
**recomm** | Option<**i32**> | 统计指定推荐组件 |  |[default to 0]
**widget** | Option<**i32**> | 统计指定组件 |  |[default to 0]
**start_date** | Option<**String**> | 统计起始日期 |  |
**end_date** | Option<**String**> | 统计结束日期 |  |

### Return type

[**Vec<serde_json::Value>**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## stats_period_widgets

> serde_json::Value stats_period_widgets(app, widget, start_date, end_date, interval)
获取模板与组件的统计信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | Option<**i32**> | 统计指定模板或组件 |  |[default to 0]
**start_date** | Option<**String**> | 统计起始日期 |  |
**end_date** | Option<**String**> | 统计结束日期 |  |
**interval** | Option<**String**> | 统计间隔 - 日、周、月、季度、年 |  |[default to date]

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


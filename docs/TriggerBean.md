# TriggerBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | Option<**i32**> |  | [optional]
**url** | Option<**String**> |  | [optional]
**triggers** | Option<**String**> |  | [optional]
**password** | Option<**String**> |  | [optional]
**all_triggers** | Option<**Vec<String>**> |  | [optional]
**enabled** | Option<**bool**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# SynonymsBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**r#type** | Option<**i32**> |  | [optional]
**words** | Option<**Vec<String>**> |  | [optional]
**synonyms** | Option<**Vec<String>**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



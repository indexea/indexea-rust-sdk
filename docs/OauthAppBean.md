# OauthAppBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**account** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**ident** | Option<**String**> | 三方应用标识 client id | [optional]
**name** | Option<**String**> |  | [optional]
**intro** | Option<**String**> |  | [optional]
**logo** | Option<**String**> |  | [optional]
**url** | Option<**String**> | 应用地址 | [optional]
**secret** | Option<**String**> | 第三方应用密钥, 该值仅在创建和重置密钥时返回 | [optional]
**callback** | Option<**String**> | 回调地址 | [optional]
**scopes** | Option<**String**> | 授权范围 | [optional]
**created_at** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



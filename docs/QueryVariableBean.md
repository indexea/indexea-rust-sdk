# QueryVariableBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**name** | Option<**String**> | 变量名 | [optional]
**pname** | Option<**String**> | 变量对应 HTTP 请求的参数名 | [optional]
**intro** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> | 变量类型 | [optional]
**required** | Option<**bool**> |  | [optional]
**format** | Option<**String**> |  | [optional]
**value** | Option<**String**> |  | [optional]
**values** | Option<**String**> | 变量可用值列表，以逗号分隔 | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



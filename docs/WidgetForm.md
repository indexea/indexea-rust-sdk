# WidgetForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**r#type** | **String** |  | [default to Web]
**sub_type** | Option<**String**> |  | [optional][default to None]
**intro** | Option<**String**> |  | [optional]
**queries** | [**Vec<crate::models::QueryBean>**](QueryBean.md) |  | 
**layout** | Option<**String**> |  | [optional]
**status** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



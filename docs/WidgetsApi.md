# \WidgetsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**widget_period_copy**](WidgetsApi.md#widget_period_copy) | **POST** /widget/{app}/copy | 复制指定组件并创建新组件
[**widget_period_copy_to_widget**](WidgetsApi.md#widget_period_copy_to_widget) | **PUT** /widget/{app}/copy | 复制组件到已有组件
[**widget_period_create**](WidgetsApi.md#widget_period_create) | **POST** /widgets/{app} | 创建组件
[**widget_period_delete**](WidgetsApi.md#widget_period_delete) | **DELETE** /widgets/{app}/{widget} | 删除组件
[**widget_period_detail**](WidgetsApi.md#widget_period_detail) | **GET** /widget/{ident} | 获取UI组件的所有相关信息
[**widget_period_download**](WidgetsApi.md#widget_period_download) | **GET** /widgets/{app}/{widget}/download | 下载组件应用源码
[**widget_period_get**](WidgetsApi.md#widget_period_get) | **GET** /widgets/{app}/{widget} | 获取组件的详情
[**widget_period_list**](WidgetsApi.md#widget_period_list) | **GET** /widgets/{app} | 获取应用的组件列表
[**widget_period_logo**](WidgetsApi.md#widget_period_logo) | **POST** /widgets/{app}/{widget}/logo | 设置组件 Logo
[**widget_period_update**](WidgetsApi.md#widget_period_update) | **PUT** /widgets/{app}/{widget} | 修改组件
[**widget_period_update_settings**](WidgetsApi.md#widget_period_update_settings) | **PATCH** /widgets/{app}/{widget} | 修改组件设置参数



## widget_period_copy

> crate::models::WidgetBean widget_period_copy(app, widget)
复制指定组件并创建新组件

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 源组件编号 | [required] |

### Return type

[**crate::models::WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_copy_to_widget

> bool widget_period_copy_to_widget(app, widget, to)
复制组件到已有组件

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 源组件编号 | [required] |
**to** | **i32** | 目标组件编号 | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_create

> crate::models::WidgetBean widget_period_create(app, widget_form)
创建组件

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget_form** | [**WidgetForm**](WidgetForm.md) |  | [required] |

### Return type

[**crate::models::WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_delete

> bool widget_period_delete(app, widget, password)
删除组件

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 组件编号 | [required] |
**password** | **String** |  | [required] |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_detail

> crate::models::WidgetBean widget_period_detail(ident, x_token)
获取UI组件的所有相关信息

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**ident** | **String** | UI组件的唯一标识 | [required] |
**x_token** | Option<**String**> | 如果要使用非发布的组件，需要组件作者授权 |  |

### Return type

[**crate::models::WidgetBean**](WidgetBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_download

> std::path::PathBuf widget_period_download(app, widget, framework)
下载组件应用源码

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 组件编号 | [required] |
**framework** | **String** | 指定的技术框架 | [required] |

### Return type

[**std::path::PathBuf**](std::path::PathBuf.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: default

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_get

> crate::models::WidgetBean widget_period_get(app, widget)
获取组件的详情

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 组件编号 | [required] |

### Return type

[**crate::models::WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_list

> Vec<crate::models::WidgetBean> widget_period_list(app)
获取应用的组件列表

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |

### Return type

[**Vec<crate::models::WidgetBean>**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_logo

> crate::models::WidgetLogo widget_period_logo(app, widget, logo)
设置组件 Logo



### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 组件编号 | [required] |
**logo** | Option<**std::path::PathBuf**> |  |  |

### Return type

[**crate::models::WidgetLogo**](WidgetLogo.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_update

> crate::models::WidgetBean widget_period_update(app, widget, widget_form)
修改组件

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 组件编号 | [required] |
**widget_form** | [**WidgetForm**](WidgetForm.md) |  | [required] |

### Return type

[**crate::models::WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## widget_period_update_settings

> bool widget_period_update_settings(app, widget, key, r#type, value, vcode)
修改组件设置参数

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**app** | **String** | 应用标识 | [required] |
**widget** | **i32** | 组件编号 | [required] |
**key** | **String** | 配置项名称 | [required] |
**r#type** | **String** | 配置值类型 | [required] |
**value** | **String** | 配置值 | [required] |
**vcode** | Option<**String**> | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) |  |

### Return type

**bool**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


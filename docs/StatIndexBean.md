# StatIndexBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | Option<**i32**> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**date** | Option<[**String**](string.md)> |  | [optional]
**storage** | Option<**i32**> |  | [optional]
**searchs** | Option<**i32**> |  | [optional]
**errors** | Option<**i32**> |  | [optional]
**records** | Option<**i32**> |  | [optional]
**avg_time** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# KeywordBindingBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**i32**> |  | [optional]
**app** | Option<**i32**> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**query** | Option<**i32**> |  | [optional]
**keyword** | Option<**String**> |  | [optional]
**alias** | Option<**Vec<String>**> |  | [optional]
**ids** | Option<**Vec<String>**> |  | [optional]
**created_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



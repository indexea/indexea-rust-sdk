/*
 * Indexea OpenAPI
 *
 * OpenAPI of Indexea
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 * Generated by: https://openapi-generator.tech
 */

/// PaymentInvoice : 发票信息



#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct PaymentInvoice {
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<i32>,
    #[serde(rename = "account", skip_serializing_if = "Option::is_none")]
    pub account: Option<i32>,
    #[serde(rename = "app", skip_serializing_if = "Option::is_none")]
    pub app: Option<i32>,
    #[serde(rename = "orders", skip_serializing_if = "Option::is_none")]
    pub orders: Option<Vec<crate::models::PaymentOrder>>,
    #[serde(rename = "price", skip_serializing_if = "Option::is_none")]
    pub price: Option<i32>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<i32>,
    #[serde(rename = "status", skip_serializing_if = "Option::is_none")]
    pub status: Option<i32>,
    #[serde(rename = "created_at", skip_serializing_if = "Option::is_none")]
    pub created_at: Option<String>,
    #[serde(rename = "logs", skip_serializing_if = "Option::is_none")]
    pub logs: Option<Vec<String>>,
}

impl PaymentInvoice {
    /// 发票信息
    pub fn new() -> PaymentInvoice {
        PaymentInvoice {
            id: None,
            account: None,
            app: None,
            orders: None,
            price: None,
            r#type: None,
            status: None,
            created_at: None,
            logs: None,
        }
    }
}



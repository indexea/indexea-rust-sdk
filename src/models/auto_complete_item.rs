/*
 * Indexea OpenAPI
 *
 * OpenAPI of Indexea
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AutoCompleteItem {
    #[serde(rename = "value", skip_serializing_if = "Option::is_none")]
    pub value: Option<String>,
    #[serde(rename = "url", skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[serde(rename = "props", skip_serializing_if = "Option::is_none")]
    pub props: Option<serde_json::Value>,
}

impl AutoCompleteItem {
    pub fn new() -> AutoCompleteItem {
        AutoCompleteItem {
            value: None,
            url: None,
            props: None,
        }
    }
}



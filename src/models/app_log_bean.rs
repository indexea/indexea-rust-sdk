/*
 * Indexea OpenAPI
 *
 * OpenAPI of Indexea
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 * Generated by: https://openapi-generator.tech
 */

/// AppLogBean : 单条日志信息



#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AppLogBean {
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<i32>,
    #[serde(rename = "app", skip_serializing_if = "Option::is_none")]
    pub app: Option<i32>,
    #[serde(rename = "account", skip_serializing_if = "Option::is_none")]
    pub account: Option<Box<crate::models::AppLogAccount>>,
    #[serde(rename = "index", skip_serializing_if = "Option::is_none")]
    pub index: Option<i32>,
    #[serde(rename = "widget", skip_serializing_if = "Option::is_none")]
    pub widget: Option<i32>,
    #[serde(rename = "query", skip_serializing_if = "Option::is_none")]
    pub query: Option<i32>,
    #[serde(rename = "recomm", skip_serializing_if = "Option::is_none")]
    pub recomm: Option<i32>,
    /// 日志类型值
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<i32>,
    /// 日志类型说明
    #[serde(rename = "type_desc", skip_serializing_if = "Option::is_none")]
    pub type_desc: Option<String>,
    /// 日志内容
    #[serde(rename = "log", skip_serializing_if = "Option::is_none")]
    pub log: Option<String>,
    #[serde(rename = "detail", skip_serializing_if = "Option::is_none")]
    pub detail: Option<serde_json::Value>,
    #[serde(rename = "ip", skip_serializing_if = "Option::is_none")]
    pub ip: Option<String>,
    #[serde(rename = "created_at", skip_serializing_if = "Option::is_none")]
    pub created_at: Option<String>,
}

impl AppLogBean {
    /// 单条日志信息
    pub fn new() -> AppLogBean {
        AppLogBean {
            id: None,
            app: None,
            account: None,
            index: None,
            widget: None,
            query: None,
            recomm: None,
            r#type: None,
            type_desc: None,
            log: None,
            detail: None,
            ip: None,
            created_at: None,
        }
    }
}



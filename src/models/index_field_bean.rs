/*
 * Indexea OpenAPI
 *
 * OpenAPI of Indexea
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: indexea.com@gmail.com
 * Generated by: https://openapi-generator.tech
 */

/// IndexFieldBean : 索引和字段列表信息



#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct IndexFieldBean {
    #[serde(rename = "index", skip_serializing_if = "Option::is_none")]
    pub index: Option<crate::models::IndexBean>,
    /// 字段列表信息
    #[serde(rename = "fields", skip_serializing_if = "Option::is_none")]
    pub fields: Option<serde_json::Value>,
}

impl IndexFieldBean {
    /// 索引和字段列表信息
    pub fn new() -> IndexFieldBean {
        IndexFieldBean {
            index: None,
            fields: None,
        }
    }
}


